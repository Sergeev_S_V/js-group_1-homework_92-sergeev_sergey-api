const express = require('express');
const cors = require('cors');
const app = express();
const expressWs = require('express-ws')(app);

const port = 8000;

app.use(cors());

const clients = {};
let pixels = [];

app.ws('/chat', (ws, req) => {
  const id = req.get('sec-websocket-key');
  clients[id] = ws;

  console.log('Client connected.');
  console.log('Number of active connections: ', Object.values(clients).length);

  ws.send(JSON.stringify(
    {
      type: 'NEW_IMAGE',
      pixels: pixels
    }
  ));

  ws.on('message', async (msg) => {
    console.log('Client sent message: ', msg);
    let decodedMessage;
    try {
      decodedMessage = JSON.parse(msg);
    } catch (e) {
      return ws.send(JSON.stringify({
        type: 'ERROR',
        message: 'Message is not JSON'
      }));
    }

    switch (decodedMessage.type) {
      case 'CREATE_IMAGE':
        pixels = pixels.concat(decodedMessage.pixels);
        Object.values(clients).forEach(client => {
          client.send(JSON.stringify({
            type: 'NEW_IMAGE',
            pixels: pixels,
          }));
        });
        break;
      default:
        return ws.send(JSON.stringify({
          type: 'ERROR',
          message: 'Unknown message type'
        }));
    }

  });

  ws.on('close', (msg) => {
    delete clients[id];
    console.log('Client disconnected.');
    console.log('Number of active connections: ', Object.values(clients).length);
  });
});

app.listen(port, () => {
  console.log(`Server started on ${port} port!`);
});